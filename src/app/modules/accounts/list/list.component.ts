import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { EditAccountDialog } from '../edit/edit.component';
import { AccountService } from '../../../core/services/in_mem_services/account.service';
import { AccountData } from '../account';

@Component({
	selector: 'app-account-list',
	templateUrl: './list.component.html',
	styleUrls: ['./list.component.styl']
})
export class ListComponent implements OnInit {

	displayedColumns: string[] = ['name', 'date-of-birth', 'actions'];
	accounts: AccountData[];

	constructor(public dialog: MatDialog, private accountService: AccountService) { }

	ngOnInit() {
		// load accounts
		this.accountService.getAccounts().subscribe((data : AccountData[])=>{
			this.accounts = data;
		})
	}

	instanceOfAccount(object: any): object is Account {
	    return object;
	}

	openEditDialog(account): void {
		const dialogRef = this.dialog.open(EditAccountDialog, {
			disableClose: true,
			width: '600px',
			data: account
		});

		dialogRef.afterClosed().subscribe(result => {
			// update account
			if(result != undefined && this.instanceOfAccount(result)) Object.keys(account).map(function(key) {account[key] = result[key]})
		});
	}
}
