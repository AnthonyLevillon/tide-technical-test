import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { AccountData } from '../account';
import { AccountService } from '../../../core/services/in_mem_services/account.service';

@Component({
	selector: 'app-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.styl']
})
export class EditAccountDialog {
	submitted = false;
	errorMessage = '';
	accountForm: FormGroup;

	constructor(public dialogRef: MatDialogRef<EditAccountDialog>, @Inject(MAT_DIALOG_DATA) public data: AccountData, private formBuilder: FormBuilder, private accountService: AccountService) {
		this.accountForm = this.formBuilder.group({
			id: [this.data.id, Validators.required],
			name: [this.data.name, Validators.required],
			dateOfBirth: [this.data.dateOfBirth, Validators.required]
		});
	}

	onNoClick(): void {
		this.dialogRef.close();
	}

	// convenience getter for easy access to form fields
	get f() { return this.accountForm.controls; }

	onSubmit() {
		this.submitted = true;

		// stop here if form is invalid
		if (this.accountForm.invalid) {
			return;
		} else {
			// update the account using the fake account service API
			this.accountService.updateAccount(this.accountForm.value).subscribe(
				data => this.dialogRef.close(this.accountForm.value),
				error => this.errorMessage = error
			);
		}

	}

}
