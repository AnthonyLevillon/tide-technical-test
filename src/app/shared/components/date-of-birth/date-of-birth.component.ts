import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';

class Range implements Iterable<number> {
	constructor(
		public readonly low: number,
		public readonly high: number,
		public readonly step: number = 1
		) {
	}

	*[Symbol.iterator]() {
		for (let x = this.low; x <= this.high; x += this.step) {
			yield x;
		}
	}
}

@Component({
	selector: 'tide-date-of-birth',
	templateUrl: './date-of-birth.component.html',
	styleUrls: ['./date-of-birth.component.styl']
})
export class DateOfBirthComponent implements OnInit {

	@Input() dateOfBirth: Date;
	@Input() formGroup: FormGroup;

  	@Output() dateOfBirthChange: EventEmitter<string>  = new EventEmitter<string>();

	controlDay: FormControl;
	controlMonth: FormControl;
	controlYear: FormControl;

	// initial day range (will be updated)
	dayRange = new Range(1, 31);

	months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	yearRange = new Range(1900, (new Date()).getFullYear());

	constructor() { }

	ngOnInit() {
		// convert dateOfBirth to date
		this.dateOfBirth = new Date(this.dateOfBirth);

		this.initFormControls();

		this.onChanges();
	}

	initFormControls(): void{
		this.controlDay = new FormControl(this.dateOfBirth.getDate(), Validators.required);
		this.controlMonth = new FormControl(this.dateOfBirth.getMonth(), Validators.required);
		this.controlYear = new FormControl(this.dateOfBirth.getFullYear(), Validators.required);

		this.formGroup.addControl('controlDay', this.controlDay);
		this.formGroup.addControl('controlMonth', this.controlMonth);
		this.formGroup.addControl('controlYear', this.controlYear);
	}

	onChanges(): void {
		this.formGroup.valueChanges.subscribe(val => {
			let tmpMonth = val.controlMonth+1
			let maxDay = new Date(val.controlYear, tmpMonth, 0).getDate();
			if(val.controlDay > maxDay){
				// update dayRange
				this.dayRange = new Range(1, maxDay)
				// update controlDay and the FormGroup
				val.controlDay = maxDay;
				this.formGroup.patchValue({controlDay:maxDay})
			}
			// emit the updated value to the parent
			this.dateOfBirthChange.emit(tmpMonth + '/' + val.controlDay + '/' + val.controlYear);
		})
	}

}
