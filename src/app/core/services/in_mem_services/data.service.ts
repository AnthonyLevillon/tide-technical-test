import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
	providedIn: 'root'
})
export class DataService implements InMemoryDbService {

	constructor() { }
	createDb(){

		let  accounts =  [
			{ "id": 1, "name": "Mitko Mitkoff", "dateOfBirth": "10/3/1980" },
			{ "id": 2, "name": "Mr Pattel", "dateOfBirth": "10/2/1994" },
			{ "id": 3, "name": "Georgi Georgiev", "dateOfBirth": "4/2/2002" },
			{ "id": 4, "name": "Ivo Ivanoff", "dateOfBirth": "3/2/1999" },
			{ "id": 5, "name": "Todor Todoroff", "dateOfBirth": "11/2/1994" },
			{ "id": 6, "name": "Test user 1", "dateOfBirth": "10/23/1968" },
			{ "id": 7, "name": "Stefan Stefanov", "dateOfBirth": "12/14/1952" },
			{ "id": 8, "name": "Spass S Spassoff", "dateOfBirth": "11/29/1964" },
			{ "id": 9, "name": "Ivan Ivanov", "dateOfBirth": "11/19/1915" },
			{ "id": 10, "name": "Mr Daniel S", "dateOfBirth": "2/29/1976" }
		];

		return {accounts};

	}
}