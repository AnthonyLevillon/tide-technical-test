import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
	providedIn: 'root'
})
export class AccountService {

	SERVER_URL: string = "http://localhost:8080/api/";
	constructor(private httpClient: HttpClient) { }

	public getAccounts(){ 
		return this.httpClient.get(this.SERVER_URL + 'accounts');
	}

	public updateAccount(account: {id: number, name: number, dateOfBirth: Date}){
		return this.httpClient.put(`${this.SERVER_URL + 'accounts'}/${account.id}`, account);
	}

	public getAccount(accountId){
		return this.httpClient.get(`${this.SERVER_URL + 'accounts'}/${accountId}`); 
	}

	public createAccount(account: {id: number, name: number, dateOfBirth: Date}){
		return this.httpClient.post(`${this.SERVER_URL}` + 'accounts', account);
	}

	public deleteAccount(accountId){
		return this.httpClient.delete(`${this.SERVER_URL + 'accounts'}/${accountId}`);
	}

}
