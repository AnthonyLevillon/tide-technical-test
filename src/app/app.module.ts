import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListComponent } from './modules/accounts/list/list.component';
import { EditAccountDialog } from './modules/accounts/edit/edit.component';
import { HeaderComponent } from './core/header/header.component';
import { MatToolbarModule, MatInputModule, MatDialogModule, MatButtonModule, MatSelectModule, MatIconModule, MatTableModule } from '@angular/material';
import { InMemoryWebApiModule } from "angular-in-memory-web-api";  
import { DataService } from "./core/services/in_mem_services/data.service";
import { DateOfBirthComponent } from './shared/components/date-of-birth/date-of-birth.component';  

@NgModule({
	exports: [
		MatToolbarModule,
		MatInputModule,
		MatDialogModule,
		MatButtonModule,
		MatSelectModule,
		MatIconModule,
		MatTableModule,
		BrowserAnimationsModule
	],
	declarations: [
		AppComponent,
		ListComponent,
		HeaderComponent,
		EditAccountDialog,
		DateOfBirthComponent
	],
	entryComponents: [
		EditAccountDialog
	],
	imports: [
		BrowserModule,
		HttpClientModule,
		MatToolbarModule,
		MatInputModule,
		MatDialogModule,
		MatButtonModule,
		MatSelectModule,
		MatIconModule,
		MatTableModule,
		BrowserAnimationsModule,
		ReactiveFormsModule,
		InMemoryWebApiModule.forRoot(DataService),
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
