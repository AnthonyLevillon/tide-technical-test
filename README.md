# TideTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.8.

## Development server

Run `ng serve --open` for a dev server. Automatically open `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## REST and API Services

- CRUD Accounts service `src/app/core/services/in_mem_services/account.service.ts`.

## MOCK data

- Fake REST API using angular-in-memory-web-api